<?php 

require_once('Animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal("shaun");

echo "nama = " . $sheep -> name ."<br>"; // "shaun"
echo "legs =" . $sheep->legs ."<br>"; // 4
echo "cold blooded =" .$sheep->cold_blooded. "<br> <br>" ; // "no"


$kodok = new Frog("buduk");
echo "nama = " . $kodok -> name ."<br>"; 
echo "Jumlah kali =" . $kodok->kaki2 ."<br>"; 
echo "cold blooded =" .$kodok->cold_blooded. "<br>";
echo "Jump = ". $kodok-> jump. " <br> <br>";


$sungokong = new Ape("kera sakti");
echo "nama = " . $sungokong -> name ."<br>"; 
echo "Jumlah kali =" . $sungokong->kaki ."<br>"; 
echo "cold blooded =" .$sungokong->cold_blooded. "<br>";
echo "yell = ". $sungokong-> yell; 

?>